
function hideAllBut(allSelector, butSelector)
{
  var allElems = qsa(allSelector),
      butElem = qs(butSelector);

  for(var i = 0, len = allElems.length; i < len; i++)
  {
    allElems[i].style.display = "none";
  }

  butElem.style.display = "";
}

function MCOA(tasksSelector)
{
  this.tasks = qsa(tasksSelector);
  this.answers = qsa(tasksSelector + " .answer");
  this.currentTask = qs(tasksSelector + "-1");
  this.correctNum = 0;
  this.onFinish = undefined;

  hideAllBut(tasksSelector, tasksSelector + "-1");

  var that = this;

  this.answers.addEventListener("click", function(){
    var currAnswer = this;

    if(hasClassName(currAnswer, "correct"))
    {
      addClassName(currAnswer, "true-answer");
      var correctNum = qs(".correct-num");
      that.correctNum ++;
      correctNum.innerHTML = that.correctNum;
    }
    else
    {
      addClassName(currAnswer, "false-answer");
      addClassName(find(currAnswer.parentNode, ".correct"), "true-answer");
    }

    timeout[0] = setTimeout(function(){
      var nextTask = that.currentTask.nextElementSibling;

      that.currentTask.style.display = "none";
      nextTask.style.display = "";

      that.currentTask = nextTask;

      if(that.onFinish && hasClassName(that.currentTask, "m-comment"))
      {
        that.onFinish();
      }
    }, 2000);
  });
}

MCOA.prototype.onEnd = function(func){
  this.onFinish = func;
};

function MCMA(taskSelector)
{
  this.task = qs(taskSelector),
  this.choiceLabels = findAll(this.task, "label"),
  this.checkboxes = findAll(this.task, "input"),
  this.trueAnswersNum = findAll(this.task, ".true").length,
  this.onFinish = undefined;
  this.checkButton = find(this.task, ".check-button");

  var that = this;

  that.checkButton.style.display = "none";

  that.choiceLabels.addEventListener("click", function(){
    var currCheckbox = this.previousElementSibling;
    currCheckbox.checked = !currCheckbox.checked;

    var event = document.createEvent("HTMLEvents");
    event.initEvent("change", true, false);
    currCheckbox.dispatchEvent(event);
  });

  that.checkboxes.addEventListener("change", function(){
    if(this.checked)
    {
      if(!that.trueAnswersNum)
        this.checked = false;
      else
        that.trueAnswersNum --;
    }
    else
    {
        that.trueAnswersNum ++;
    }

    if(!that.trueAnswersNum)
      that.checkButton.style.display = "";
    else
      that.checkButton.style.display = "none";

    find(that.task, ".answer-num").innerHTML = that.trueAnswersNum;
  });

  that.checkButton.addEventListener("click", function(){
    var boxes = that.checkboxes;

    for(var i = 0, len = boxes.length; i < len; i++)
    {
      if(boxes[i].checked)
      {
        if(hasClassName(boxes[i], "true"))
          addClassName(boxes[i].nextElementSibling, "true-answer")
        else
          addClassName(boxes[i].nextElementSibling, "false-answer")
      }
      else
      {
        if(hasClassName(boxes[i], "true"))
          addClassName(boxes[i].nextElementSibling, "should-have")
      }
    }

    if(that.onFinish)
    {
      that.onFinish();
    }
  });
}

MCMA.prototype.onEnd = function(func)
{
  this.onFinish = func;
}
