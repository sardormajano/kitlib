function Mahjong(buttonsSelector)
{
  this.buttons = qsa(buttonsSelector),
  this.activeButton = undefined,
  this.congratulations = undefined;

  var that = this;

  that.buttons.addEventListener("click", function(){
    var self = this;

    if(!that.activeButton)
    {
      that.activeButton = self;
      addClassName(that.activeButton, "active-button");
    }
    else
    {
      addClassName(self, "active-button");

      if(self.getAttribute("data-code") === that.activeButton.getAttribute("data-code")
          && self !== that.activeButton)
      {
        addClassName(self, "transition-s flipped");
        addClassName(that.activeButton, "transition-s flipped");
        that.activeButton = undefined;

        if(qsa(".flipped").length === that.buttons.length &&
            that.congratulations)
        {
          that.congratulations();
        }
      }
      else
      {
        addClassName(self, "false");
        addClassName(that.activeButton, "false");
        timeout[0] = setTimeout(function(){
          removeClassName(self, "false active-button");
          removeClassName(that.activeButton, "false active-button");
          that.activeButton = undefined;
        }, 1000);
      }
    }
  });
}

Mahjong.prototype.setCongratulations = function(func)
{
  this.congratulations = func;
}
