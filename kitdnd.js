function DnD(dElem)
{
  var ev,
      xDifference = 0,
      yDifference = 0;

  this.dragElem = dElem;
  this.dragElem.dragging = false;

  var that = this;

  makeDraggable();

  function move()
  {
     var  xMouse = parseInt(ev.clientX),
          yMouse = parseInt(ev.clientY);

    that.dragElem.style.left = (xMouse - xDifference) + "px",
    that.dragElem.style.top = (yMouse - yDifference) + "px";
  }

  function makeDraggable()
  {
    that.dragElem.style.cursor = "all-scroll";
    that.dragElem.style.position = "absolute";
    addClassName(that.dragElem, "noselect");

    that.dragElem.addEventListener("mousedown", function(event){
      that.dragElem.style.zIndex = "100";

      ev = event;
      that.dragElem.dragging = true;

      var offset = that.dragElem.getBoundingClientRect(),
          xElement = Math.floor(offset.left),
          yElement = Math.floor(offset.top),
          xMouse = parseInt(ev.clientX),
          yMouse = parseInt(ev.clientY),
          xMain = parseInt(getComputedStyle(qs("main"))["left"]),
          yMain = parseInt(getComputedStyle(qs("main"))["top"]);

      xDifference = xMouse - xElement + xMain;
      yDifference = yMouse - yElement + yMain;
    });

    window.addEventListener("mousemove", function(event){
      if(that.dragElem.dragging)
      {
        ev = event;
        move();
      }
    });

    window.addEventListener("mouseup", function(){
      that.dragElem.dragging = false;
      that.dragElem.style.zIndex = "";
    });
  }
}

DnD.prototype.onDragStart = function(handler)
{
  this.dragElem.addEventListener("mousedown", handler);
}

DnD.prototype.onDragEnd = function(handler)
{
  this.dragElem.addEventListener("mouseup", handler);
}

DnD.prototype.onDragMove = function(handler)
{
    this.dragElem.addEventListener("mousemove", handler);
}
