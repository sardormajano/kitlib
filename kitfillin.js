function FillinTask(taskSelector)
{
  this.inputs = qsa(taskSelector + " input"),
  this.checkButton = qs(taskSelector + " .check-button"),
  this.onFinish = undefined;

  var that = this;

  that.checkButton.style.display = "none";

  for(var i = 0, len = that.inputs.length; i < len; i ++)
  {
    currInput = that.inputs[i];
    currDataTrue = currInput.getAttribute("data-true");
    currInput.setAttribute("size", currDataTrue.length);
  }

  that.inputs.addEventListener("keyup", function(event){
    noneIsEmpty() ? that.checkButton.style.display = "" :
                    that.checkButton.style.display = "none";
  });

  function noneIsEmpty()
  {
    for(var i = 0, len = that.inputs.length; i < len; i++)
    {
      if(!that.inputs[i].value.length)
        return false;
    }

    return true;
  }

  that.checkButton.addEventListener("click", function(){
    var allInputs = that.inputs;

    for(var i = 0, len = allInputs.length; i < len; i++)
    {
      if(allInputs[i].value.toLowerCase() === allInputs[i].getAttribute("data-true").toLowerCase())
        addClassName(allInputs[i], "true-input");
      else
      {
        addClassName(allInputs[i], "false-input");
        allInputs[i].value = allInputs[i].getAttribute("data-true");
      }
    }

    if(that.onFinish)
      that.onFinish();
  });
}

FillinTask.prototype.onEnd = function(func)
{
  this.onFinish = func;
}

function main(){
  var fillinTask = new FillinTask(".fillin-task");

  fillinTask.onEnd(function(){
    timeout[0] = setTimeout(function(){
      addClassName(qs(".fillin-task"), "transition-2s flipped-180");
    }, 5000);
  });
};
ready(main);
