function DnDBasketBall(ballSelector)
{
  this.balls = qsa(ballSelector),
  this.dragBalls = [],
  this.congratulations,
  this.onFail = undefined,
  this.onSuccess = undefined;

  var that = this,
      ballNumber = this.balls.length;

  //making them draggable
  for(var i = 0, len = this.balls.length; i < len; i++)
  {
    that.dragBalls[i] = new DnD(this.balls[i]);
  }
  for(var i = 0, len = that.dragBalls.length; i < len; i++)
  {
    (function(j){
      that.dragBalls[j].onDragEnd(function(event){
        that.dragBalls[j].dragElem.style.display = "none";

        var currVegetable = that.dragBalls[j].dragElem,
            currBasket = document.elementFromPoint(event.clientX, event.clientY);

        if(currVegetable.getAttribute("data-code")
          === currBasket.getAttribute("data-code"))
        {
          currBasket.innerHTML = currVegetable.innerHTML;
          currBasket.style.fontSize = getComputedStyle(currVegetable)["font-size"];
          currBasket.style.fontFamily = getComputedStyle(currVegetable)["font-family"];
          currBasket.style.color = getComputedStyle(currVegetable)["color"];
          currBasket.style.backgroundColor = getComputedStyle(currVegetable)["background-color"];
          currBasket.style.backgroundImage = getComputedStyle(currVegetable)["background-image"];

          currVegetable.parentNode.removeChild(currVegetable);
          ballNumber --;

          if(that.onSuccess)
            that.onSuccess();

          if(!ballNumber)
          {
            that.congratulations();
          }
        }
        else
        {
            currVegetable.style.left = "";
            currVegetable.style.top = "";
            currVegetable.style.display = "";

            if(that.onFail)
              that.onFail();
        }
      });
    })(i);
  }
}

DnDBasketBall.prototype.setCongratulations = function(func)
{
  this.congratulations = func;
}

DnDBasketBall.prototype.setOnFail = function(func)
{
  this.onFail = func;
}

DnDBasketBall.prototype.setOnSuccess = function(func)
{
  this.onSuccess = func;
}

function DnDSort(ballSelector)
{
  this.balls = qsa(ballSelector),
  this.dragBalls = [],
  this.rectangles = [],
  this.freePosition = {},
  this.congratulations;

  var that = this;

  for(var i = 0, len = this.balls.length; i < len; i++)
  {
    (function(j){
      that.dragBalls[j] = new DnD(that.balls[j]);

      that.dragBalls[j].onDragStart(function(event){
        var self = that.dragBalls[j],
            parent = this.parentNode;
        that.freePosition.left = (parseFloat(getComputedStyle(self.dragElem)["left"])
                  / parseFloat(getComputedStyle(parent)["width"]) * 100) + "%";
        that.freePosition.top = (parseFloat(getComputedStyle(self.dragElem)["top"])
                  / parseFloat(getComputedStyle(parent)["height"]) * 100) + "%";
      });

      that.dragBalls[j].onDragEnd(function(event){
        this.style.left = that.freePosition.left;
        this.style.top = that.freePosition.top;

        if(checkLocations())
        {
          that.congratulations();
        }
      });

      that.dragBalls[j].onDragMove(function(event){
        var self = that.dragBalls[j];

        if(!self.dragElem.dragging)
          return ;

        self.dragElem.style.display = "none";

        var elemUnder = document.elementFromPoint(event.clientX, event.clientY);

        if(hasClassName(elemUnder, "ball"))
        {
          var parent = this.parentNode,
              tempLeft = (parseFloat(getComputedStyle(elemUnder)["left"])
                        / parseFloat(getComputedStyle(parent)["width"]) * 100) + "%",
              tempTop = (parseFloat(getComputedStyle(elemUnder)["top"])
                        / parseFloat(getComputedStyle(parent)["height"]) * 100) + "%";

          elemUnder.style.left = that.freePosition.left;
          elemUnder.style.top = that.freePosition.top;

          var tempLocation = elemUnder.getAttribute("data-curr-loc");
          elemUnder.setAttribute("data-curr-loc", self.dragElem.getAttribute("data-curr-loc"));
          self.dragElem.setAttribute("data-curr-loc", tempLocation);

          that.freePosition.left = tempLeft;
          that.freePosition.top = tempTop;
        }

        self.dragElem.style.display = "";
      });
    })(i);
  }

  function checkLocations()
  {
    var sorted = true;

    for(var i = 0, len = that.balls.length; i < len; i++)
    {
      if(that.balls[i].getAttribute("data-curr-loc")
       !== that.balls[i].getAttribute("data-true-loc"))
       {
          addClassName(that.balls[i], "false-ball")
          sorted = false;
       }
       else
       {
         addClassName(that.balls[i], "true-ball")
       }
    }

    timeout[0] = setTimeout(function(){
      for(var i = 0, len = that.balls.length; i < len; i++)
      {
        removeClassName(that.balls[i], "false-ball");
        removeClassName(that.balls[i], "true-ball");
      }
    }, 1000);

    return sorted;
  }
}

DnDSort.prototype.setCongratulations = function(func)
{
  this.congratulations = func;
}

/*function main()
{
  var ballSelector = ".ball",
      sortTask = new DnDSort(ballSelector);

  var congratulations = function()
  {
    console.log("good job!!");
  }

  sortTask.setCongratulations(congratulations);
}

ready(main);*/
