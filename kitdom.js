var qs = document.querySelector.bind(document),
    qsa = document.querySelectorAll.bind(document),

    timeout = [];

Element.prototype.qs = Element.prototype.querySelector,
Element.prototype.qsa = Element.prototype.querySelectorAll;

NodeList.prototype.addEventListener = function(event, eventListener)
{
  var that = this;
  for(var i = 0, len = that.length; i < len; i++)
  {
    (function(j){
      that[j].addEventListener(event, eventListener);
    })(i)
  }
}

function hide(element)
{
  element.style.display = "none";
}

function show(element)
{
  element.style.display = "";
}

function toggle(element)
{
  element.style.display === "none" ?
    element.style.display = "" : element.style.display = "none";
}

function find(element, child)
{
    return element.querySelector(child);
}

function findAll(element, child)
{
    return element.querySelectorAll(child);
}

function getPosition(element)
{
  var boundingClientRect = element.getBoundingClientRect(),
      body = document.body,
      docElement = document.documentElement;

  var pageXOffset = window.pageXOffset || docElement.scrollLeft || body.scrollLeft,
      pageYOffset = window.pageYOffset || docElement.scrollTop || body.scrollTop;

  var clientTop = docElement.clientTop || body.clientTop,
      clientLeft = docElement.clientLeft || body.clientLeft;

  var top = boundingClientRect.top + pageYOffset - clientTop,
      left = boundingClientRect.left + pageXOffset - clientLeft;

  return {
    "left" : Math.round(left),
    "top" : Math.round(top)
  }
}

function addClassName(element, className)
{
  if(element.classList)
  {
    if(className.indexOf(" ") > -1)
    {
      var classNames = className.split(" ");

      for(var i = 0, len = classNames.length; i < len; i++)
      {
        element.classList.add(classNames[i]);
      }
    }
    else
    {
      element.classList.add(className);
    }
  }
  else
  {
    element.className += " " + className;
  }
}

function removeClassName(element, className)
{
  if (element.classList)
  {
    if(className.indexOf(" ") > -1)
    {
      var classNames = className.split(" ");

      for(var i = 0, len = classNames.length; i < len; i++)
      {
        element.classList.remove(classNames[i]);
      }
    }
    else
    {
      element.classList.remove(className);
    }
  }
  else
    element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
}

function hasClassName(element, className)
{
  if (element.classList)
    return element.classList.contains(className);
  else
    return new RegExp('(^| )' + className + '( |$)', 'gi').test(element.className);
}

function toggleClassName(element, className)
{
  if(hasClassName(element, className))
  {
    removeClassName(element, className);
  }
  else
  {
    addClassName(element, className);
  }
}

function addEventsListener(events, element, func)
{
  var eventsArray = typeof events === "string" ? events.split(" ") : events;
  for(var i = 0; i < eventsArray.length; i++)
  {
    element.addEventListener(eventsArray[i],func)
  }
}

function ready(fn)
{
  if(document.readyState !== 'loading')
  {
    fn();
  }
  else
  {
    document.addEventListener("DOMContentLoaded", fn);
  }
}

function scaleMain()
{
  var mainContainer = qs("main"),
      bodyWidth = parseInt(getComputedStyle(document.body)["width"]),
      bodyHeight = parseInt(getComputedStyle(document.body)["height"]),
      mainHeight, mainWidth, mainTop = 0, mainLeft = 0;

  if(bodyWidth <= bodyHeight * 2)
  {
    mainWidth = bodyWidth;
    mainHeight = Math.floor(bodyWidth / 2);
    mainTop = Math.floor((bodyHeight - mainHeight) / 2);
  }
  else
  {
    mainHeight = bodyHeight;
    mainWidth = bodyHeight * 2;
    mainLeft = Math.floor((bodyWidth - mainWidth) / 2);
  }

  mainContainer.style.width = mainWidth + "px";
  mainContainer.style.height = mainHeight + "px";
  mainContainer.style.left = mainLeft + "px";
  mainContainer.style.top = mainTop + "px";
  document.body.style.fontSize = parseInt(mainWidth / 100) + "px";
}

window.addEventListener("resize", scaleMain);

ready(scaleMain);
